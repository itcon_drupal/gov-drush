LAP (LAMP without MySQL) on Docker - DRUSH CRON CONTAINER
===

This is a docker container used on the USDA Enterprise Web Application Platform Service (EWAPS). It is designed to integrated with a Rancher-based orchestration system for the deployment and maintenance of web sites.  

This is a cron container designed to run cron on Drupal web sites. It runs the standard drupal cron at the interval specified. The environment variables are nearly identical to that of `gov-drupal` with an addition for the cron interval.

Last Updated: February 6, 2020  

Environment Variables
---

**DOCROOT**  
_The document root where Apache will be pointed. Should correlate roughly to the same as `GIT_DIR` however `DOCROOT` includes the directory inside GIT_DIR that has the site files in it._  

**GIT_DIR**  
_This is the path where the git repository will be placed._  

**GIT_URL**  
_The URL of the git repository that will be downloaded. Should include any and all credentials for authenticating into the repository._  

**GIT_BRANCH**  
_The branch in the git repository to be checked out._  

**GRAV**  
_Is this web site powered by Grav? Set to 1 if yes, leave undefined if no._  

**CRON_INTERVAL**  
_The cron interval. This is in the format of `m h d m w`