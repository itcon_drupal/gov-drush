FROM centos:7

# Set our our meta data for this container.
LABEL name="LAP Container for eWAPS Cron Runs"
LABEL vendor="United States Department of Agriculture"

ENV PATH /usr/local/src/vendor/bin/:/usr/local/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/application/docroot/vendor/drush/drush:vendor/drush/drush

# Set TERM env to avoid mysql client error message "TERM environment variable not set" when running from inside the container
ENV TERM xterm

# Fix command line compile issue with bundler.
ENV LC_ALL en_US.utf8

# Install and enable repositories epel and remi.
RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
    rpm -Uvh https://repo.ius.io/ius-release-el7.rpm && \
    yum -y update

RUN yum -y install \
    curl \
    git \
    mariadb \
    msmtp \
    net-tools \
    python34 \
    vim \
    wget \
    rsync \
    unzip \
    zip \
    gcc \
    gcc-c++ \
    make \
    mod_ssl.x86_64 \
    ImageMagick \
    ghostscript \
    cronie.x86_64 \
    gettext

# Install PHP modules
RUN yum-config-manager --enable remi-php73 && \
  yum -y install \
    php \
    php-cli \
    php-curl \
    php-gd \
    php-imap \
    php-mbstring \
    php-mysqlnd \
    php-odbc \
    php-pear \
    php-pecl-imagick \
    php-pecl-json \
    php-pecl-redis4 \
    php-pecl-memcached \
    php-pecl-uploadprogress \
    php-pecl-apcu \
    php-opcache \
    php-bcmath \
    php-xml \
    php-ldap \
    php-devel

RUN pecl install igbinary igbinary-devel redis

# Perform yum cleanup
RUN yum -y upgrade && \
    yum clean all

# Install Composer and Drush
RUN curl -sS https://getcomposer.org/installer | php -- \
    --install-dir=/usr/local/bin \
    --filename=composer

# Disable services management by systemd.
RUN systemctl disable httpd.service

# Apache config, and PHP config, test apache config
# See https://github.com/docker/docker/issues/7511 /tmp usage
COPY public/index.php /var/www/public/index.php
COPY centos-7 /tmp/centos-7/

RUN rsync -a /tmp/centos-7/etc/ /etc/ 

# Simple startup script to avoid some issues observed with container restart
ADD conf/run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

COPY cron/drupal /etc/cron.d/drupal
RUN chmod 0644 /etc/cron.d/drupal

ADD conf/mail.ini /etc/php.d/mail.ini
RUN chmod 644 /etc/php.d/mail.ini

CMD ["/run-httpd.sh"]
